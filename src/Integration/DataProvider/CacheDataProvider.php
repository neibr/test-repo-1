<?php

namespace src\Integration\DataProvider;

use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use src\Integration\DataProvider\Exception\FetchException;

class CacheDataProvider implements DataProviderInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var DataProviderInterface
     */
    private $dataProvider;

    /**
     * @var CacheItemPoolInterface
     */
    private $cache;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $cacheTtl;

    /**
     * @param DataProviderInterface $dataProvider
     * @param CacheItemPoolInterface $cache
     * @param LoggerInterface $logger
     * @param string $cacheTtl
     */
    public function __construct(
        DataProviderInterface $dataProvider,
        CacheItemPoolInterface $cache,
        LoggerInterface $logger,
        string $cacheTtl
    ) {
        $this->dataProvider = $dataProvider;
        $this->cache = $cache;
        $this->setLogger($logger);
        $this->cacheTtl = $cacheTtl;
    }

    /**
     * @inheritdoc
     *
     * @throws FetchException
     */
    public function fetch(array $request): array
    {
        try {
            $cacheKey = $this->generateCacheKey($request);
            $cacheItem = $this->cache->getItem($cacheKey);
            if ($cacheItem->isHit()) {
                return $cacheItem->get();
            }

            $result = $this->dataProvider->fetch($request);

            $cacheItem
                ->set($result)
                ->expiresAt(
                    (new DateTime())->modify("+{$this->cacheTtl} seconds")
                );

            return $result;
        } catch (Exception $e) {
            $this->logger->critical("Error occured during fetching data, reason '{$e->getMessage()}'");

            throw new FetchException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @param array $request
     *
     * @return string
     */
    private function generateCacheKey(array $request): string
    {
        return json_encode($request);
    }
}
