<?php

namespace src\Integration\DataProvider;

interface DataProviderInterface
{
    /**
     * @param array $request
     *
     * @return array
     */
    public function fetch(array $request): array;
}
